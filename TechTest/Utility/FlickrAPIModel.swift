//
//  FlickrAPIModel.swift
//  TechTest
//
//  Created by Mike Lee on 22/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit

class FlickrAPIModel: NSObject {
    
    private let FLICKR_KEY:String = "6eb19dc66ffa11914d0fbaac62008608"
    private let FLICKR_URLSTR:String = "https://api.flickr.com/services/rest/"
    private let FLICKR_PERPAGE:Int = 50

    enum FlickrErrors: Int {
        
        case tooManyTags = 1
        case unknownUser
        case parameterlessDisabled
        case noPoolPermission
        case userDeleted
        case apiUnavaliable = 10
        case noValidMachineTags
        case exceededMaxMachineTags
        case canOnlySearchUrContacts = 17
        case illogicalArgs
        case invalidAPIKey = 100
        case serviceUnavaolable = 105
        
       /*
        106: Write operation failed
        The requested operation failed due to a temporary issue.
        111: Format "xxx" not found
        The requested response format was not found.
        112: Method "xxx" not found
        The requested method was not found.
        114: Invalid SOAP envelope
        The SOAP envelope send in the request could not be parsed.
        115: Invalid XML-RPC Method Call
        The XML-RPC request document could not be parsed.
        116: Bad URL found*/
    }
    
    static let shared = FlickrAPIModel()
    
    typealias FlickrPhotoSearchResponse = (NSError?, [FlickrPhotoVO]?, Int) -> Void
    typealias FlickrPhotoInfoResponse = (NSError?, FlicrPhotoInfoVO?) -> Void
    
    override init() {
        
    }
    
    func fetchPhotosFrom(searchText: String, searchTag: String?, page: Int, onCompletion: @escaping FlickrPhotoSearchResponse) {
        
        //create query url
        let strEscapedSearchText: String = searchText.addingPercentEncoding(withAllowedCharacters:.urlHostAllowed)!
        let strUrlString: String = "\(FLICKR_URLSTR)?method=flickr.photos.search&api_key=\(FLICKR_KEY)&text=\(strEscapedSearchText)&per_page=\(FLICKR_PERPAGE)&page=\(page)&format=json&nojsoncallback=1" + ((searchTag != nil) ? "&tags=\(searchTag!)":"")
        
        let pUrl: NSURL = NSURL(string: strUrlString)!
        let pSearchTask = URLSession.shared.dataTask(with: pUrl as URL, completionHandler: {data, response, error -> Void in
            
            //an error occured, return the error to closure
            if error != nil {
                onCompletion(error as NSError?, nil, 0)
                return
            }
            
            do {
                
                //parse the json result
                let resultsDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
                guard let results = resultsDictionary else { return }
                
                //fetch Flickr error code if any
                if let statusCode = results["code"] as? Int {
                    
                    let pFlickrError = NSError(domain: "com.flickr.api", code: statusCode, userInfo: nil)
                    onCompletion(pFlickrError, nil, 0)
                    return
                }
                
                guard let pPhotosContainer = resultsDictionary!["photos"] as? NSDictionary else { return }
                guard let pPhotosArray = pPhotosContainer["photo"] as? [NSDictionary] else { return }
                guard let intTotalPage = pPhotosContainer["pages"] as? Int else { return }
                
                //fetch photo detail from response data
                let aryFlickrPhotos: [FlickrPhotoVO] = pPhotosArray.map { pPhotoDictionary in
                    
                    let strPhotoId = pPhotoDictionary["id"] as? String ?? ""
                    let strFarm = pPhotoDictionary["farm"] as? Int ?? 0
                    let strSecret = pPhotoDictionary["secret"] as? String ?? ""
                    let strServer = pPhotoDictionary["server"] as? String ?? ""
                    let strTitle = pPhotoDictionary["title"] as? String ?? ""
                    
                    let pFlickrPhotoVO = FlickrPhotoVO(photoId: strPhotoId, farm: strFarm, secret: strSecret, server: strServer, title: strTitle, info: nil)
                    return pFlickrPhotoVO
                }
                
                onCompletion(nil, aryFlickrPhotos,intTotalPage)
                
            } catch let error as NSError {
                onCompletion(error, nil, 0)
            }
            
        })
        pSearchTask.resume()
    }

    func fetchPhotoInfoFrom(id: String, onCompletion: @escaping FlickrPhotoInfoResponse) {
        
        let strUrlString: String = "\(FLICKR_URLSTR)?method=flickr.photos.getInfo&api_key=\(FLICKR_KEY)&photo_id=\(id)&format=json&nojsoncallback=1"
        let pUrl: NSURL = NSURL(string: strUrlString)!
        let pSearchTask = URLSession.shared.dataTask(with: pUrl as URL, completionHandler: {data, response, error -> Void in
            
            //an error occured, return the error to closure
            if error != nil {
                onCompletion(error as NSError?, nil)
                return
            }
            
            do {
                
                //parse the json result
                let resultsDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
                guard let results = resultsDictionary else { return }
                
                //fetch Flickr error code if any
                if let statusCode = results["code"] as? Int {
                    
                    let pFlickrError = NSError(domain: "com.flickr.api", code: statusCode, userInfo: nil)
                    onCompletion(pFlickrError, nil)
                    return
                }
                
                guard let aryPhotosContainer = resultsDictionary!["photo"] as? NSDictionary else { return }
                guard let aryPhotosDate = aryPhotosContainer["dates"] as? NSDictionary else { return }
                
                guard let aryPhotoTagsContainer = aryPhotosContainer["tags"] as? NSDictionary else {return}
                guard let aryPhotoTagsArray = aryPhotoTagsContainer["tag"] as? [NSDictionary] else {return}
                guard let aryTitleContainer = aryPhotosContainer["title"] as? NSDictionary else {return}
                let strTitle: String = aryTitleContainer["_content"] as! String
                //fetch photo detail from response data
                
                let aryPhotosTagsList: [String] = aryPhotoTagsArray.map { pTag in
                    
                    return pTag["raw"] as? String ?? ""
                }
                let pFlickrPhotoInfo: FlicrPhotoInfoVO = FlicrPhotoInfoVO(title: strTitle, tags: aryPhotosTagsList, date: aryPhotosDate["taken"] as! String)
                onCompletion(nil, pFlickrPhotoInfo)
                
            } catch let error as NSError {
                onCompletion(error, nil)
            }
            
        })
        pSearchTask.resume()
    }
}
