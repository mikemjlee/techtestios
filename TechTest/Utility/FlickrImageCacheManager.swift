//
//  ImageCacheManager.swift
//  TechTest
//
//  Created by Mike Lee on 24/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit

typealias CompleteHandlerBlock = (_ data: Data?) -> ()

struct FlickrDownloadItem {
    
    var url: URL
    weak var imageView: UIImageView?
    var handler: CompleteHandlerBlock?
}

enum FlickerDownloadPriority {
    case midPriority
    case highPriority
}

class FlickrImageCacheManager: NSObject, URLSessionDownloadDelegate {
    
    private let IMGCACHE_MAXCONN = 10
    
    private let IMGCACHE_DIR = "\(NSTemporaryDirectory())Flickr/"
    
    public static let shared = FlickrImageCacheManager()
    
    private var m_aryImageCacheInMem: [String: Data] = [String: Data]();
    private var m_pSession: URLSession?
    private var m_aryConnectionQueue: [URLSessionDownloadTask: FlickrDownloadItem] = [URLSessionDownloadTask: FlickrDownloadItem]()
    private var m_aryRequestWaitingQueue: [FlickrDownloadItem] = [FlickrDownloadItem]()
    
    
    override init() {
        super.init()
        if !FileManager.default.fileExists(atPath: IMGCACHE_DIR) {
            do {
                try FileManager.default.createDirectory(atPath: IMGCACHE_DIR, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Unable to create directory \(error.debugDescription)")
            }
        }
        
        var strSessionID: String? = UserDefaults.standard.string(forKey: "BCCoreSessionID")
        
        if strSessionID == nil {
            
            strSessionID = "BCCore-" + UUID().uuidString
            
            UserDefaults.standard.set(strSessionID!, forKey: "BCCoreSessionID")
            
        }
        
        UserDefaults.standard.synchronize()
        
        let objSessionConfiguration:URLSessionConfiguration = URLSessionConfiguration.background(withIdentifier: strSessionID!)
        
        objSessionConfiguration.httpMaximumConnectionsPerHost = IMGCACHE_MAXCONN
        
        m_pSession = URLSession(configuration: objSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FlickrImageCacheManager.clearMemory(_:)), name: NSNotification.Name.UIApplicationDidReceiveMemoryWarning, object: nil)
       
        NotificationCenter.default.addObserver(self, selector: #selector(FlickrImageCacheManager.cleanDisk(_:)), name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
        
    }
    
    func requestFromFlickr(image: UIImageView, url: URL, priority: FlickerDownloadPriority, onCompleted:@escaping CompleteHandlerBlock)  {
        
        let strMD5: String = url.absoluteString.md5!
        var pImageData: Data? = m_aryImageCacheInMem[strMD5]
        if pImageData != nil {
            image.image = UIImage(data: pImageData!)
        } else {
             pImageData = loadFromLocalFile(md5: strMD5)
            if pImageData != nil {
                image.image = UIImage(data:pImageData!)
            } else {
                let item: FlickrDownloadItem = FlickrDownloadItem(url: url, imageView: image, handler: onCompleted)
                downloadFromFlickr(item: item, priority: priority)
                return
            }
        }
        
        onCompleted(pImageData)
    }
    
    private func loadFromLocalFile(md5: String)->Data? {
        
        if FileManager.default.fileExists(atPath: IMGCACHE_DIR + md5) {
            
            return try! Data(contentsOf: URL(fileURLWithPath: IMGCACHE_DIR + md5))
        }
        
        return nil
    }
    
    private func downloadFromFlickr(item: FlickrDownloadItem, priority: FlickerDownloadPriority) {
    
        switch(priority) {
            case .highPriority:
                m_aryRequestWaitingQueue.insert(item, at: 0)
            case .midPriority:
                m_aryRequestWaitingQueue.append(item)
        }
        
        initConnection()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        if let pDownloadItem: FlickrDownloadItem = m_aryConnectionQueue[downloadTask] {
            let pImageData = try! Data(contentsOf: location)
            let strMD5 = downloadTask.currentRequest!.url!.absoluteString.md5!
            try! pImageData.write(to: URL(fileURLWithPath: IMGCACHE_DIR + strMD5), options: .atomic)
            pDownloadItem.imageView?.image = UIImage(data: pImageData)
            m_aryConnectionQueue[downloadTask] = nil
            m_aryImageCacheInMem[strMD5] = pImageData
            pDownloadItem.handler?(pImageData)
        }
        initConnection()
    }
    
    
    private func initConnection() {
        
        while m_aryConnectionQueue.count <= IMGCACHE_MAXCONN && m_aryRequestWaitingQueue.count > 0 {
            
            let pRequest: FlickrDownloadItem = m_aryRequestWaitingQueue.remove(at: 0)
            let objRequest:URLRequest = URLRequest(url: pRequest.url)
            let pSessionTask: URLSessionDownloadTask = m_pSession!.downloadTask(with:objRequest)
            
            //cache the uuid ,use it when download finish
            m_aryConnectionQueue[pSessionTask] = pRequest
            pSessionTask.resume()
        }
    }
    
    public func removeCache() {
        m_aryImageCacheInMem.removeAll()
    }
    
    @objc private func clearMemory(_ notification:Notification) {
        removeCache()
    }
    
    @objc private func cleanDisk(_ notification:Notification) {
        
        let pFM = FileManager.default
        do {
            let aryContents: [String] = try pFM.contentsOfDirectory(atPath: IMGCACHE_DIR)
            for strFilePath in aryContents {
                
                try pFM.removeItem(atPath: IMGCACHE_DIR + strFilePath)
            }
        } catch let error as NSError {
            print("Unable to create directory \(error.debugDescription)")
        }
    }

}
