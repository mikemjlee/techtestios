//
//  FlickrPhotoVO.swift
//  TechTest
//
//  Created by Mike Lee on 22/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit

struct FlickrPhotoVO {
    
    let photoId: String
    let farm: Int
    let secret: String
    let server: String
    let title: String
    var info: FlicrPhotoInfoVO?
    
    var photoUrlM: URL {
        return URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoId)_\(secret)_m.jpg")!
    }
    
    var photoUrlL: URL {
        return URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoId)_\(secret)_b.jpg")!
    }

}

struct FlicrPhotoInfoVO {
    
    let title: String
    let tags: [String]
    let date: String
}
