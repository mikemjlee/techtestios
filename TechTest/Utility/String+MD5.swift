//
//  String+MD5.swift
//  TechTest
//
//  Created by Mike Lee on 24/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//
import Foundation

extension String {
    
    var md5: String? {
        get {
            guard let messageData = self.data(using:String.Encoding.utf8) else { return nil }
            var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
            
            _ = digestData.withUnsafeMutableBytes {digestBytes in
                messageData.withUnsafeBytes {messageBytes in
                    CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
                }
            }
            
            let cString = digestData.map { String(format: "%02hhx", $0) }.joined()
            return cString
        }
    }
}
