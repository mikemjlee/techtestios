//
//  TechTestLocationView.swift
//  TechTest
//
//  Created by Mike Lee on 22/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit

class TechTestLocationView: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        //m_pMapView.frame = rect
         print("TechTestLocationView: self", self.frame)
        //self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[map(==superview)]|", options: [], metrics: nil, views: ["superview": self,"map":self.m_pMapView]))
        
        //self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[map(==superview)]|", options: [], metrics: nil, views: ["superview": self,"map":self.m_pMapView]))
    }
 
    
    private var m_pMapView: UIImageView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false
        m_pMapView = UIImageView.init(image: UIImage(named: "ttlocation_img_map"))
        m_pMapView.contentMode = .scaleAspectFit
        m_pMapView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.addSubview(m_pMapView)
        
        print("TechTestLocationView: m_pMapView", m_pMapView.frame)
        
        print("TechTestLocationView: self", self.frame)
        
        self.backgroundColor = UIColor.clear
        
        // add constraints to fill the parent frame size
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[map(==superview)]|", options: [], metrics: nil, views: ["superview": self,"map":self.m_pMapView]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[map(==superview)]|", options: [], metrics: nil, views: ["superview": self,"map":self.m_pMapView]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //create and add map view into current view
        translatesAutoresizingMaskIntoConstraints = false
        m_pMapView = UIImageView.init(image: UIImage(named: "ttlocation_img_map"))
        m_pMapView.contentMode = .scaleAspectFit
        m_pMapView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.addSubview(m_pMapView)
        
        print("TechTestLocationView: m_pMapView", m_pMapView.frame)
        
        print("TechTestLocationView: self", self.frame)
        
        self.backgroundColor = UIColor.clear
        
        // add constraints to fill the parent frame size
       //self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[map(==superview)]|", options: [], metrics: nil, views: ["superview": self,"map":self.m_pMapView]))
        
        //self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[map(==superview)]|", options: [], metrics: nil, views: ["superview": self,"map":self.m_pMapView]))
    }

}
