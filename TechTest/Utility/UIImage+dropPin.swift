//
//  UIPinMapImage.swift
//  TechTest
//
//  Created by Mike Lee on 26/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit

extension UIImage {
    func dropPin(longitude: CGFloat, latitude: CGFloat, diameter: CGFloat, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.saveGState()
        
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        
        // since the image is not start at 180 longtitude and 90 latitude, adjust the coordinate on image,
        let x = (176.5 + longitude) / 360.0 * self.size.width
        let y = (90 - latitude) / 180.0 * self.size.height + 80
        let rect = CGRect(x: x, y: y, width: diameter, height: diameter)
        ctx.setFillColor(color.cgColor)
        ctx.fillEllipse(in: rect)
        
        ctx.restoreGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return img
    }
}
