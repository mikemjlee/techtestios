//
//  EntirelyImageViewController.swift
//  TechTest
//
//  Created by Mike Lee on 23/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit

class EntirelyImageViewController: UIViewController {

    @IBOutlet weak var m_pEntirelyImage: UIImageView!
    
    @IBOutlet weak var m_pImageName: UILabel?
    
    @IBOutlet weak var n_pFileSize: UILabel?
    
    @IBOutlet weak var n_pDate: UILabel?
    
    private var m_pCacheFlickrVO: FlickrPhotoVO?
    
    private var m_strFileSize: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //n_pFileSize.text = String(format: "%d x %d (%@)", Int(m_pCacheImage!.size.width * m_pCacheImage!.scale), Int(m_pCacheImage!.size.height * m_pCacheImage!.scale), m_strFileSize ?? "0KB")
        
        //m_pEntirelyImage.image = m_pCacheImage
        loadFlickrPhoto()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func unwindButtonTapped(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "unwindToSearchWithSegue", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setFlickrPhoto(vo: FlickrPhotoVO) {
        
        m_pCacheFlickrVO = vo
    }
    
    func loadFlickrPhoto() {
        
        FlickrImageCacheManager.shared.requestFromFlickr(image: m_pEntirelyImage, url: m_pCacheFlickrVO!.photoUrlL, priority: .highPriority,onCompleted: {(data : Data?) -> Void in
            let pImage = self.m_pEntirelyImage.image
            
            let strFileSize = (data != nil) ? (data!.count > 1024 * 1024) ? String(format:"%.1f MB", Double(data!.count) / 1024.0 / 1024.0) : (data!.count > 1024 ) ? String(format:"%.1f KB", Double(data!.count) / 1024.0) : String(format:"%d bytes", data!.count) : "0KB"
            
            self.n_pFileSize?.text = String(format: "%d x %d (%@)", Int(pImage!.size.width * pImage!.scale), Int(pImage!.size.height * pImage!.scale), strFileSize)
            self.m_pImageName?.text = self.m_pCacheFlickrVO?.info?.title
            self.n_pDate?.text = self.m_pCacheFlickrVO?.info?.date
        })
        /*var pImageData: Data = try! Data(contentsOf: vo.photoUrlL)
        m_pCacheImage = UIImage(data: pImageData)
        print(pImageData.count)
        m_strFileSize = (pImageData.count > 1024 * 1024) ? String(format:"%.1f MB", Double(pImageData.count) / 1024.0 / 1024.0) : (pImageData.count > 1024 ) ? String(format:"%.1f KB", Double(pImageData.count) / 1024.0) : String(format:"%d bytes", pImageData.count)*/
    //}
    
    }
    
    //hide status bar
    override var prefersStatusBarHidden : Bool {
        return true
    }

}
