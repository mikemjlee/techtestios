//
//  FlickrSearchResultCell.swift
//  TechTest
//
//  Created by Mike Lee on 22/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit



public let CELL_IKD = "cell_ikd"

class FlickrSearchResultCell: UICollectionViewCell {

    private let THUMBNAIL_BORDER:CGFloat = 6
    private let BOTTOM_HEIGHT:CGFloat = 20
    
    var m_pThumbnailView:UIImageView?
    
    func buildLayoutWith(photo: FlickrPhotoVO) {
        
        m_pThumbnailView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height) )
        m_pThumbnailView?.contentMode = .scaleAspectFit
        m_pThumbnailView?.layer.borderWidth = THUMBNAIL_BORDER
        m_pThumbnailView?.layer.borderColor = UIColor.white.cgColor
        self.addSubview(m_pThumbnailView!)
        FlickrImageCacheManager.shared.requestFromFlickr(image: m_pThumbnailView!, url: photo.photoUrlM, priority: .midPriority, onCompleted: {(_ data: Data?) -> Void in })
    }
    
    override func prepareForReuse()
    {
        m_pThumbnailView?.removeFromSuperview()
        m_pThumbnailView = nil
    }
}
