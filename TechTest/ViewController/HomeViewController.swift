//
//  HomeViewController.swift
//  TechTest
//
//  Created by Mike Lee on 22/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit
import CoreLocation

class HomeViewController: UIViewController, CLLocationManagerDelegate {
    
    private var m_pLocationManager = CLLocationManager()
    private var m_pGeoCoder = CLGeocoder()
    
    @IBOutlet weak var m_pLblLocation: UILabel?
    @IBOutlet weak var m_pMapView: UIImageView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.m_pLocationManager.delegate = self
        
        switch CLLocationManager.authorizationStatus() {
           
            case .notDetermined:
                // Request Location authorization
                m_pLocationManager.requestWhenInUseAuthorization()
            
            case .restricted, .denied:
                // Show alert
                print("Location authorization denied")
            
            case .authorizedWhenInUse, .authorizedAlways:
                // update location
                m_pLocationManager.startUpdatingLocation()
        }
        
        m_pMapView?.image = UIImage(named: "ttlocation_img_map")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch CLLocationManager.authorizationStatus() {
            
            case .authorizedWhenInUse, .authorizedAlways:
                m_pLocationManager.startUpdatingLocation()
            case .restricted, .denied:
                print("Location authorization denied")
            default:
                break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        m_pLocationManager.stopUpdatingLocation()
        let pLatestLocation: CLLocation = locations[locations.count - 1]
        m_pGeoCoder.reverseGeocodeLocation(pLatestLocation, completionHandler: { (placemarks: [CLPlacemark]?, error: Error?) -> Void in
            if error != nil{
                return
            }
            
            if let placemark = placemarks?[0] {
                
                self.m_pLblLocation?.text = "in " + ((placemark.country != nil) ? "\(placemark.country!)," : "") + ((placemark.administrativeArea != nil) ? "\(placemark.administrativeArea!)," : "") + (placemark.locality ?? "")
            }
        })
        
        let t = UIImage(named: "ttlocation_img_map")
        if t != nil  {
            
            m_pMapView?.image = t?.dropPin(longitude: CGFloat(pLatestLocation.coordinate.longitude), latitude: CGFloat(pLatestLocation.coordinate.latitude), diameter: 15, color: UIColor.black)
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch CLLocationManager.authorizationStatus() {
         
            case .authorizedWhenInUse, .authorizedAlways:
                m_pLocationManager.startUpdatingLocation()
            case .restricted, .denied:
                print("Alert")
            default:
            break
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //hide status bar
    override var prefersStatusBarHidden : Bool {
        return true
    }

}
