//
//  SearchViewController.swift
//  TechTest
//
//  Created by Mike Lee on 22/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    private let SEARCHVIEW_SCOPEALLTAG: String = "All"
    private let SEARCHVIEW_SCOPEMAXNUM: Int = 4
    
    @IBOutlet weak var m_pSearchPhotos: UICollectionView!
    @IBOutlet weak var m_pPicker: UIPickerView!
    @IBOutlet weak var m_pSearchBar: UISearchBar!
    
    
    //cache filtered photos
    private var m_aryFilteredPhotos: [FlickrPhotoVO]?
    private var m_aryPhotos: [FlickrPhotoVO] = [FlickrPhotoVO]()
    
    private var m_aryTagsCounter: [String: Int] = [String: Int]()
    private var m_aryFilterTitle: [String] = [String]()
    
    private var m_nSelectedIndex: Int = 0
    private var m_nCurrentSearchPage: Int = 1
    private var m_nTotalSearchPage: Int = 0
    
    private var m_strSearchTxt: String = ""
    private var m_strSearchTag: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resetAll()
    }

    private func resetAll() {
        m_aryPhotos.removeAll()
        m_pSearchPhotos.reloadData()
        m_aryTagsCounter.removeAll()
        m_aryFilterTitle.removeAll()
        m_aryFilteredPhotos = nil
        m_pSearchBar.scopeButtonTitles = m_aryFilterTitle
        m_pPicker.isHidden = true
        m_pSearchBar.showsSearchResultsButton = false
        m_pSearchBar.sizeToFit()
        m_pSearchBar.resignFirstResponder()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.setNeedsUpdateConstraints()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
        if selectedScope >= m_aryFilterTitle.count || m_aryFilterTitle[selectedScope] == SEARCHVIEW_SCOPEALLTAG {
            m_aryFilteredPhotos = nil
        } else {
        
            m_aryFilteredPhotos = [FlickrPhotoVO]()
            for pPhotos in m_aryPhotos {
                if pPhotos.info != nil && pPhotos.info!.tags.contains(m_aryFilterTitle[selectedScope]) {
                    m_aryFilteredPhotos?.append(pPhotos)
                }
            }
        }
        
        m_pSearchPhotos.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        resetAll()
        m_pSearchBar.setShowsCancelButton(true, animated: true)
        if searchBar.text != nil && m_strSearchTxt != searchBar.text! {
            m_strSearchTxt = searchBar.text!
            performSearchWithText(searchText: searchBar.text!, searchTag: nil)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return m_aryFilterTitle.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return m_aryFilterTitle[row]
    }
    
    // Catpure the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if m_aryFilterTitle[row] != SEARCHVIEW_SCOPEALLTAG && m_strSearchTag != m_aryFilterTitle[row]{
            m_strSearchTag = m_aryFilterTitle[row]
            performSearchWithText(searchText: m_pSearchBar.text!, searchTag: m_aryFilterTitle[row])
            resetAll()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        m_strSearchTxt = ""
        m_strSearchTag = ""
        m_pSearchBar.setShowsCancelButton(false, animated: true)
        m_pSearchBar.text = nil
        resetAll()
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        if m_aryFilterTitle.count > 0 {
            m_pPicker.isHidden = !m_pPicker.isHidden
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return m_aryFilteredPhotos?.count ?? m_aryPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let pCell:FlickrSearchResultCell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IKD, for: indexPath) as! FlickrSearchResultCell
        pCell.buildLayoutWith(photo: m_aryFilteredPhotos?[indexPath.item] ?? m_aryPhotos[indexPath.item])

        return pCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        // do further on search on other page
        if indexPath.row == m_aryPhotos.count-1  {
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        m_nSelectedIndex = indexPath.item
        self.performSegue(withIdentifier: "showEntirelyImg", sender: self)
    }
    
    private func performSearchWithText(searchText: String, searchTag: String?) {
        m_nTotalSearchPage = 1
        FlickrAPIModel.shared.fetchPhotosFrom(searchText: searchText, searchTag: searchTag, page: m_nCurrentSearchPage, onCompletion: { (error: NSError?, flickrPhotos: [FlickrPhotoVO]?, totalPage: Int) -> Void in
            if error == nil {
                self.m_nTotalSearchPage = totalPage
                self.m_aryPhotos = flickrPhotos!
                for i in 0 ..< self.m_aryPhotos.count {
                    FlickrAPIModel.shared.fetchPhotoInfoFrom(id: self.m_aryPhotos[i].photoId, onCompletion: { (error: NSError?, photoInfo: FlicrPhotoInfoVO?) -> Void in
                        
                        //incase that photo or cache has been deleted after search
                        if photoInfo != nil &&  self.m_aryPhotos.count > i {
                            self.m_aryPhotos[i].info = photoInfo
                            self.updateSearchScope(tags: photoInfo!.tags)
                        } else if self.m_aryPhotos.count > i{
                            self.m_aryPhotos.remove(at: i)
                        }
                    })
                }
            } else {
                self.m_aryPhotos.removeAll()
                
            }
            DispatchQueue.main.async(execute: { () -> Void in
                //self.title = searchText
                //self.tableView.reloadData()
                self.m_pSearchPhotos.reloadData()
            })
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEntirelyImg"
        {
            let pEntirelyImgVC = segue.destination as? EntirelyImageViewController
            
            if pEntirelyImgVC != nil {
                
                pEntirelyImgVC?.setFlickrPhoto(vo: m_aryFilteredPhotos?[m_nSelectedIndex] ?? m_aryPhotos[m_nSelectedIndex])
                
            }
            
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    private func updateSearchScope(tags: [String]) {
        
        //count the tags and choose most frequently tags
        if tags.count > 0 {
            
            let strTag = tags[0]
            if m_aryTagsCounter[strTag] == nil {
                m_aryTagsCounter[strTag] = 1
            } else {
                m_aryTagsCounter[strTag]! += 1
            }
        
            let arySortedTagsCounter = m_aryTagsCounter.sorted { (arg0, arg1)  in
                let (_, valueB) = arg1
                let (_, valueA) = arg0
                return valueA > valueB
            }
        
            var aryFilter:[String] = [SEARCHVIEW_SCOPEALLTAG]
        
            for strKey in arySortedTagsCounter {
                aryFilter.append(strKey.key)
                if aryFilter.count > SEARCHVIEW_SCOPEMAXNUM {
                    break
                }
            }
        
            m_aryFilterTitle = aryFilter
        
            if m_aryFilterTitle.count > 1 {
                DispatchQueue.main.async {
                    self.m_pSearchBar.scopeButtonTitles = self.m_aryFilterTitle
                    self.m_pSearchBar.showsSearchResultsButton = true
                    self.m_pSearchBar.showsScopeBar = true
                    self.m_pSearchBar.sizeToFit()
                    self.m_pPicker.reloadAllComponents()
                    self.m_pSearchBar.selectedScopeButtonIndex = 0
                }
            }
        }
    }
    
    @IBAction func unwindToSearch(segue:UIStoryboardSegue) {}
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //hide status bar
    override var prefersStatusBarHidden : Bool {
        return true
    }

}
