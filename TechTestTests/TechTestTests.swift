//
//  TechTestTests.swift
//  TechTestTests
//
//  Created by Mike Lee on 22/06/2018.
//  Copyright © 2018 Mike Lee. All rights reserved.
//

import XCTest
@testable import TechTest

class TechTestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testFlickrSearch() {
        
        
        let expectation = XCTestExpectation(description: "SomeService does stuff and runs the callback closure")
        
        var nFulfillmentCount: Int = frickSearchWithTag(expectation: expectation)
        nFulfillmentCount += frickSearchWithoutTag(expection: expectation)
        
        expectation.expectedFulfillmentCount = nFulfillmentCount
        // 3. Wait for the expectation to be fulfilled
        wait(for: [expectation], timeout: 1000.0)
    }
    
    
    func testFlickrPhotoInfo() {
        
        let expectation = XCTestExpectation(description: "Unit test fro fetch photo info")
        expectation.expectedFulfillmentCount = 2
        
        FlickrAPIModel.shared.fetchPhotosFrom(searchText: "test", searchTag: nil, page: 1,onCompletion: { (error: NSError?, flickrPhotos: [FlickrPhotoVO]?, totalPage: Int) -> Void in
            
            if flickrPhotos != nil && flickrPhotos!.count > 0 {
                
                FlickrAPIModel.shared.fetchPhotoInfoFrom(id: flickrPhotos![0].photoId,onCompletion: { (error: NSError?, flickrPhotoInfo: FlicrPhotoInfoVO?) -> Void in
                    
                    if flickrPhotoInfo != nil {
                        expectation.fulfill()
                    }
                })
            }
        })
        
        FlickrAPIModel.shared.fetchPhotoInfoFrom(id: "0",onCompletion: { (error: NSError?, flickrPhotoInfo: FlicrPhotoInfoVO?) -> Void in
            
            if error != nil {
                expectation.fulfill()
            }
        })
        
        // 3. Wait for the expectation to be fulfilled
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testFlickrFetchImage() {
        
        let expectation = XCTestExpectation(description: "Test fetch Photo from FlickrImageCacheManager ")
        
        FlickrAPIModel.shared.fetchPhotosFrom(searchText: "test", searchTag: nil, page: 1,onCompletion: { (error: NSError?, flickrPhotos: [FlickrPhotoVO]?, totalPage: Int) -> Void in
            
            let pUIImageView: UIImageView = UIImageView()
            if flickrPhotos != nil && flickrPhotos!.count > 0 {
                
                FlickrImageCacheManager.shared.requestFromFlickr(image: pUIImageView, url: flickrPhotos![0].photoUrlM, priority: .midPriority, onCompleted: {(_ data: Data?)->Void in
                    expectation.fulfill()
                })
            }
        })
        
        // 3. Wait for the expectation to be fulfilled
        wait(for: [expectation], timeout: 10.0)
    }
    
    func frickSearchWithoutTag(expection : XCTestExpectation)->Int {
        
        FlickrAPIModel.shared.fetchPhotosFrom(searchText: "test", searchTag: nil, page: 1,onCompletion: { (error: NSError?, flickrPhotos: [FlickrPhotoVO]?, totalPage: Int) -> Void in
            
            if flickrPhotos != nil && flickrPhotos!.count > 0 {
                expection.fulfill()
            }
        })
        
        return 1
    }
    
    func frickSearchWithTag(expectation : XCTestExpectation)->Int{
        
        FlickrAPIModel.shared.fetchPhotosFrom(searchText: "test", searchTag: "test", page: 1,onCompletion: { (error: NSError?, flickrPhotos: [FlickrPhotoVO]?, totalPage: Int) -> Void in
            
            if flickrPhotos != nil && flickrPhotos!.count > 0 {
                FlickrAPIModel.shared.fetchPhotoInfoFrom(id: flickrPhotos![0].photoId,onCompletion: { (error: NSError?, flickrPhotoInfo: FlicrPhotoInfoVO?) -> Void in
                    
                    if flickrPhotoInfo != nil {
                        
                        for strTag in flickrPhotoInfo!.tags {
                            
                            if strTag.lowercased() == "test" {
                                expectation.fulfill()
                                break
                            }
                        }
                    }
                })
            }
        })
        return 1
    }
    
}
